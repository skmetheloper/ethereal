exports.handler = async (event) => {
  return {
    headers: {
      'access-control-allow-origin': '*',
    },
    statusCode: 200,
    body: JSON.stringify(Date.now()),
  };
};
